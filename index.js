/**
 * @format
 */

import {AppRegistry} from 'react-native';
import LightboxExample from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => LightboxExample);
